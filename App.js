import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import persistor, { store } from './src/redux/store';
import BaseNavigator from "./src/routes/BaseNavigator";

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BaseNavigator />
        </PersistGate>
      </Provider>
    );
  }
};

export default App;