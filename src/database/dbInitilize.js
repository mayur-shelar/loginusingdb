export class DatabaseInitialization {
    // Perform any updates to the database schema. These can occur during initial configuration, or after an app store update.
    // This should be called each time the database is opened.
    updateDatabaseTables(database) {
        let dbVersion = 0;
        console.log("Beginning database updates...");
        // First: create tables if they do not already exist
        return database
            .transaction(this.createTables)
            .then(() => {
                // Get the current database version
                //return this.getDatabaseVersion(database);
                return
            }).catch(error => console.log(error))
            .then(version => {
                dbVersion = version;
                //console.log("Current database version is: " + dbVersion);
                // Perform DB updates based on this version
                // This is included as an example of how you make database schema changes once the app has been shipped
                if (dbVersion < 1) {
                    // Uncomment the next line, and the referenced function below, to enable this
                    // return database.transaction(this.preVersion1Inserts);
                }
                // otherwise,
                return;
            }).catch(error => console.log(error))
            .then(() => {
                if (dbVersion < 2) {
                    // Uncomment the next line, and the referenced function below, to enable this
                    // return database.transaction(this.preVersion2Inserts);
                }
                // otherwise,
                return;
            })
            .catch(error => console.log(error));
    }
    // Perform initial setup of the database tables
    createTables(transaction) {
        // DANGER! For dev only
        const dropAllTables = false;
        if (dropAllTables) {
            transaction.executeSql("DROP TABLE IF EXISTS userInfo;");
        }

        // Contact Tables
        transaction.executeSql("CREATE TABLE IF NOT EXISTS userInfo( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "name VARCHAR," +
            "email VARCHAR NOT NULL UNIQUE," +
            "password VARCHAR, " +
            "age VARCHAR, " +
            "address VARCHAR " +
            ");");
    }
}
