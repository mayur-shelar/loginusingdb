import CryptoJS from 'crypto-js';
import SQLite from "react-native-sqlite-storage";
import { DatabaseInitialization } from "./dbInitilize";

import globals from '../common/globals';

class DatabaseImplementation {

    constructor() {
        this.dbName = globals.dbName;
        this.databaseName = this.dbName + ".db";
    }

    // Open the connection to the database
    open = () => {
        SQLite.DEBUG(true);
        SQLite.enablePromise(true);
        let databaseInstance;
        return SQLite.openDatabase({
            name: this.databaseName,
            location: "Documents"
        })
            .then(db => {
                databaseInstance = db;
                console.log("[db] Database open!");
                // Perform any database initialization or updates, if needed
                const databaseInitialization = new DatabaseInitialization();
                return databaseInitialization.updateDatabaseTables(databaseInstance);
            }).catch(error => console.log(error))
            .then(() => {
                this.database = databaseInstance;
                return databaseInstance;
            }).catch(error => console.log(error));
    }

    // Close the connection to the database
    close = () => {
        if (this.database === undefined) {
            return Promise.reject("[db] Database was not open; unable to close.");
        }
        return this.database.close().then(status => {
            console.log("[db] Database closed.");
            this.database = undefined;
        });
    }

    /* Insert new user data from signup screen */
    insertNewUser = (name, email, password, age, address) => {
        // encrypt password before saving it to the database
        const encryptedPassword = CryptoJS.AES.encrypt(password, globals.encryptionKey).toString();
        
        return this.getDatabase().then(db => {
            db.executeSql(
                "INSERT INTO "
                + this.dbName
                + " (name, email, password, age, address) VALUES (?, ?, ?, ?, ?);", [name, email.toLowerCase(), encryptedPassword, age, address]);
        }).catch(error => console.log("Error QUERY", error))
            .then((result) => {
                console.log(`[db] New user added: ${name}!`);
                return true
            }).catch(error => console.log("Error RESULT", error));
    }

    getDataFromEmail = (email) => {
        return this.getDatabase()
            .then(db => db.executeSql(`SELECT * FROM ${this.dbName} WHERE email =? ;`, [email]))
            .catch(error => console.log(error))
            .then(([results]) => {
                if (results === undefined) {
                    return [];
                }

                if (results.rows.length) {
                    let user = {};
                    user = results.rows.item(0)
                    return [user];
                } else {
                    return []
                }
            }).catch(error =>{
                 console.log(error)});
    }

    getDatabase() {
        if (this.database !== undefined) {
            return Promise.resolve(this.database);
        }
        // otherwise: open the database first
        return this.open();
    }
}

// Export a single instance of DatabaseImplementation
export const database = new DatabaseImplementation();
