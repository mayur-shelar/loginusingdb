import React from 'react';
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import * as Routes from "./Routes";

import LoginScreen from '../components/LoginScreen';
import SignupScreen from '../components/SignupScreen';
import ViewProfileScreen from '../components/ViewProfileScreen';

const Stack = createStackNavigator();

const Auth = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={Routes.LOGIN_SCREEN}>
                <Stack.Screen
                    options={{ headerShown: false, title: '' }}
                    name={Routes.LOGIN_SCREEN}
                    component={LoginScreen} />
                <Stack.Screen
                    options={{ title: "Signup" }}
                    name={Routes.SIGNUP_SCREEN}
                    component={SignupScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

const App = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    options={{
                        title: 'Profile Details'
                    }}
                    name={Routes.PROFILE_SCREEN}
                    component={ViewProfileScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

class BaseNavigator extends React.Component {
    render() {
        const { user } = this.props;
        
        return (
            <>
                {(user != undefined && user.length )? <App /> : <Auth />}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(BaseNavigator);