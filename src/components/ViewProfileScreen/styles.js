import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    viewDashLine: {
        borderStyle: 'dashed',
        borderWidth: 0.7,
        borderRadius: 1,
        borderColor: "gray"
    },
    viewContentHorizontal: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 12,
    },
    subDetailsText: {
        flex: 1.5,
        color: 'gray',
        paddingTop: Platform.OS === 'ios' ? 5 : 0,
    },
    subLabelStatus: {
        flex: 2,
        // fontWeight: 'bold', 
        paddingTop: Platform.OS === 'ios' ? 5 : 0,
        marginLeft: 10,
    },
    logoutButton: {
        height: 30,
        marginTop: 40,
        width: "100%",
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: 'red',
        justifyContent: 'center'
    },
    logoutTxt: {
        color: 'white',
        fontWeight: 'bold'
    }
});