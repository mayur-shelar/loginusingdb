import React from 'react';
import { Text, View, Pressable } from 'react-native';

import { useSelector, useDispatch } from 'react-redux';


import { resetState } from '../../redux/store';
import { styles } from './styles';

function ViewProfileScreen() {
    const dispatch = useDispatch();

    let strUser = useSelector(state => state.user);

    let user = {};

    if (strUser.length) {
        user = JSON.parse(strUser)
    }

    return (
        <View style={styles.container}>
            <View style={styles.viewContentHorizontal}>
                <Text style={styles.subDetailsText}>{"Name:"}</Text>
                <Text style={styles.subLabelStatus}> {user.name ? user.name : '--'} </Text>
            </View>
            <View style={styles.viewDashLine} />
            <View style={styles.viewContentHorizontal}>
                <Text style={styles.subDetailsText}>{"Age:"}</Text>
                <Text style={styles.subLabelStatus}> {user.age ? user.age : '--'} </Text>
            </View>
            <View style={styles.viewDashLine} />
            <View style={styles.viewContentHorizontal}>
                <Text style={styles.subDetailsText}>{"Email:"}</Text>
                <Text style={styles.subLabelStatus}> {user.email ? user.email : '--'} </Text>
            </View>
            <View style={styles.viewDashLine} />
            <View style={styles.viewContentHorizontal}>
                <Text style={styles.subDetailsText}>{"Address:"}</Text>
                <Text style={styles.subLabelStatus}> {user.address ? user.address : '--'} </Text>
            </View>
            <View style={styles.viewDashLine} />
            <Pressable
                onPress={() => dispatch(resetState())}
                style={styles.logoutButton}>
                <Text style={styles.logoutTxt}>{"LOGOUT"}</Text>
            </Pressable>
        </View>
    )
}

export default ViewProfileScreen
