import React, { useState, createRef } from 'react';
import {
    TextInput,
    View,
    Text,
    ScrollView,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
    Alert,
} from 'react-native';
import CryptoJS from 'crypto-js';
import { useDispatch } from 'react-redux';

import globals from '../../common/globals';
import { updateLoginUser, updateLoginStatus } from '../../redux/store';
import { validateEmail } from '../../common';
import { SIGNUP_SCREEN, PROFILE_SCREEN } from '../../routes/Routes';

import { database } from '../../database/dbQueries';

import { styles } from './styles';

const LoginScreen = ({ navigation }) => {
    const dispatch = useDispatch();

    const [userEmail, setUserEmail] = useState('');
    const [errorText, setErrorText] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [isEmailValid, setIsEmailValid] = useState(false);

    const passwordInputRef = createRef();

    const validateEmailAddress = (email) => {
        const isValid = validateEmail(email);

        if (!isValid) {
            setErrorText("Please enter a valid email.")
        } else {
            setErrorText("");
        }

        setIsEmailValid(isValid);

        return isValid;
    }

    const handleSubmitPress = () => {
        setErrorText('');

        if (!isEmailValid) {
            alert('Please enter valid email');
            return;
        }
        if (!userPassword) {
            alert('Please fill Password');
            return;
        }

        database.getDataFromEmail(userEmail).then((result) => {
            if (result.length) {
                const user = result[0];
                const { email, password } = user;

                //Decrypt password
                const byte = CryptoJS.AES.decrypt(password, globals.encryptionKey);
                const originalPassword = byte.toString(CryptoJS.enc.Utf8);

                if (email === userEmail.toLowerCase() && userPassword == originalPassword) {
                    dispatch(updateLoginUser(JSON.stringify(user)));
                    navigation.navigate(PROFILE_SCREEN);
                } else {
                    setErrorText("Email or Password not correct.")
                }

            } else {
                Alert.alert("Info", "Email not available. Please register.")
            }
        })
    };

    return (
        <View style={styles.mainBody}>
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}>
                <View>
                    <KeyboardAvoidingView enabled>
                        <View style={{ alignItems: 'center' }}>
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(userEmail) => {
                                    setUserEmail(userEmail);
                                    validateEmailAddress(userEmail);
                                }}
                                placeholder={"Enter Email"}
                                placeholderTextColor={"gray"}
                                autoCapitalize="none"
                                keyboardType="email-address"
                                returnKeyType="next"
                                onSubmitEditing={() =>
                                    passwordInputRef.current &&
                                    passwordInputRef.current.focus()
                                }
                                underlineColorAndroid="#f000"
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserPassword) =>
                                    setUserPassword(UserPassword)
                                }
                                placeholder={"Enter Password"}
                                placeholderTextColor={"gray"}
                                keyboardType="default"
                                ref={passwordInputRef}
                                onSubmitEditing={Keyboard.dismiss}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                underlineColorAndroid="#f000"
                                returnKeyType="next"
                            />
                        </View>
                        {errorText != '' ? (
                            <Text style={styles.errorTextStyle}>
                                {errorText}
                            </Text>
                        ) : null}
                        <TouchableOpacity
                            style={styles.buttonStyle}
                            activeOpacity={0.5}
                            onPress={handleSubmitPress}>
                            <Text style={styles.buttonTextStyle}>{'LOGIN'}</Text>
                        </TouchableOpacity>
                        <Text
                            style={styles.registerTextStyle}
                            onPress={() => navigation.navigate(SIGNUP_SCREEN)}>
                            {'New Here ? Register'}
                        </Text>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
        </View>
    );
};
export default LoginScreen;

