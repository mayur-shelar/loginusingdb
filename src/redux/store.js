import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

// TYPES
const ACTION_RESET = "ACTION_RESET";
const ACTION_UPDATE_USER = "ACTION_UPDATE_USER";
const ACTION_UPDATE_USER_LOGIN_LOGOUT = "ACTION_UPDATE_USER_LOGIN_LOGOUT";

// REDUCER
const defaultState = {
    isLoggedIn: false,
    user: "",
}

function rootReducer(state = defaultState, action) {
    switch (action.type) {
        case ACTION_UPDATE_USER_LOGIN_LOGOUT:
            return {
                ...state,
                isLoggedIn: action.payload
            }
        case ACTION_UPDATE_USER:
            return {
                ...state,
                user: action.payload
            }
        case ACTION_RESET:
            return {
                defaultState
            }
        default:
            return state
    }
}

// ACTIONS
export function updateLoginStatus(status) {
    return {
        type: ACTION_UPDATE_USER_LOGIN_LOGOUT,
        payload: status
    }
}

export function updateLoginUser(user) {
    return {
        type: ACTION_UPDATE_USER,
        payload: user
    }
}

export function resetState() {
    return {
        type: ACTION_RESET,
    }
}

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: [],
    timeout: null,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, applyMiddleware(createLogger()));

const persistor = persistStore(store);

export default persistor;